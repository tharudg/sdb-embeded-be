(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-main-cont></app-main-cont>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/main-cont/main-cont.component.html":
/*!******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/main-cont/main-cont.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"row\" style=\"margin-top:10px; margin-bottom:10px ; margin-left: 2%;margin-right: 2%\"  >\n    <div class=\"media col-8\">\n      <div style=\"padding-left: 380px; top: 235px; position: absolute;\">\n         <div class=\"spinner-border text-primary\" role=\"status\" *ngIf=\"isUploading\">\n            <span class=\"sr-only\">Loading...</span>\n          </div>\n      </div>\n       \n      <img (mousemove)=\"onMouse($event)\" (click)=\"onClikPoint()\" class=\"imageObj img-thumbnail\" [src]=\"scannedImage\" >\n      <div [class.image-hidden]=\"scannedImage==''\"\n        *ngFor=\"let point of selectedPoints; let i = index\" \n        (click)=\"onRemove(i)\" class=\"animated zoomIn\" \n        [style.left]=\"point.x + 'px'\" \n        [style.top]=\"point. y+ 'px'\" \n        style=\"width: 10px;height: 10px;position: absolute;background: red;border-radius: 50%;cursor: not-allowed\">\n      </div>\n\n\n    </div>\n    <div class=\"col-4\" style=\"margin-top: 3% ;position:relative;\">\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"getImage()\">Scan</button>\n      <div style=\"height: 380px\" > <br>\n        <h4>Selected Points</h4>\n  \n        <table mdbTable mdbTableScroll scrollY=\"true\" maxHeight=\"200\" bordered=\"true\">\n            <thead>\n              <tr>\n                <th style=\"width: 100px\" *ngFor=\"let head of headElements; let i = index\" scope=\"col\">{{head}}</th>\n              </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let point of selectedPoints; let i = index\">\n                <th>{{i+1}}</th>\n                <td>{{point.x}}</td>\n                <td>{{point.y}}</td>\n              </tr>\n            </tbody>\n          </table>\n        </div>\n          <button type=\"button\" class=\"btn btn-primary\" [disabled]= \"selectedPoints.length==0\" (click)=\"solderPoints()\">Solder</button>\n    </div>\n    <sui-dimmer class=\"page\" [isDimmed]=\"mainStore.isSoldering\" [isClickable] = \"false\">\n        <h2 class=\"ui inverted icon header\">\n            <i class=\"microchip icon\"></i>\n            <div class=\"progress\" *ngIf=\"isSoldering\" style=\"width: 800px; height:20px ; margin-left: 3%\">\n                <div class=\"progress-bar progress-bar-striped bg-warning\" role=\"progressbar\" style=\"width: 75%\" aria-valuenow=\"75\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n            </div>\n        </h2> <br>\n        <button type=\"button\" class=\"btn btn-danger\" (click)= \"abort()\">Abort</button>\n        <button type=\"button\" class=\"btn btn-success\" *ngIf =\"!mainStore.isSoldering\" (click)= \"done()\">Done</button>\n    </sui-dimmer>\n  </div>\n\n\n  \n  \n"

/***/ }),

/***/ "./src/app/Caller/caller.basic.ts":
/*!****************************************!*\
  !*** ./src/app/Caller/caller.basic.ts ***!
  \****************************************/
/*! exports provided: BaseCaller */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseCaller", function() { return BaseCaller; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _caller_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./caller.path */ "./src/app/Caller/caller.path.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var BaseCaller = /** @class */ (function () {
    function BaseCaller(
    // public authStore: AuthStore,
    router) {
        this.router = router;
        this.axios = axios__WEBPACK_IMPORTED_MODULE_1___default.a;
        this.callerPath = new _caller_path__WEBPACK_IMPORTED_MODULE_3__["CallerPath"]();
    }
    /**
     * @param {string} path
     * @param {string} data
     * @returns
     * @memberof BaseService
     */
    BaseCaller.prototype.get = function (path, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.axios
                .get(path, { params: data })
                .then(function (result) { return resolve(result.data); })
                .catch(function (err) { return reject(err); });
        });
    };
    /**
      * @param {string} path
      * @param {string} data
      * @returns
      * @memberof BaseService
      */
    BaseCaller.prototype.post = function (path, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.axios
                .post(path, data)
                .then(function (result) { return resolve(result.data); })
                .catch(function (err) { return reject(err); });
        });
    };
    /**
     * @param {string} path
     * @param {string} data
     * @returns
     * @memberof BaseService
     */
    BaseCaller.prototype.delete = function (path, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.axios
                .delete(path, data)
                .then(function (result) { return resolve(result.data.data); })
                .catch(function (err) { return reject(err); });
        });
    };
    BaseCaller.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] }
    ]; };
    BaseCaller = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
    ], BaseCaller);
    return BaseCaller;
}());



/***/ }),

/***/ "./src/app/Caller/caller.path.ts":
/*!***************************************!*\
  !*** ./src/app/Caller/caller.path.ts ***!
  \***************************************/
/*! exports provided: CallerPath */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CallerPath", function() { return CallerPath; });
var CallerPath = /** @class */ (function () {
    function CallerPath() {
        var _this = this;
        this.ip = '192.168.43.120';
        this.raspi = 'raspberrypi';
        this.prodBase = "http://" + this.ip + "/api";
        this.local = 'http://localhost:5000';
        this.base = this.prodBase;
        this.image = this.base + '/scanned/image';
        this.points = this.base + '/points';
        this.delPoints = function (id) { return [_this.base + ("/points/" + id)]; };
    }
    return CallerPath;
}());



/***/ }),

/***/ "./src/app/Caller/nodeCaller.ts":
/*!**************************************!*\
  !*** ./src/app/Caller/nodeCaller.ts ***!
  \**************************************/
/*! exports provided: NodeCaller */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NodeCaller", function() { return NodeCaller; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _caller_basic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./caller.basic */ "./src/app/Caller/caller.basic.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _caller_path__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./caller.path */ "./src/app/Caller/caller.path.ts");




var NodeCaller = /** @class */ (function () {
    function NodeCaller(caller, path) {
        this.caller = caller;
        this.path = path;
    }
    NodeCaller.prototype.getImage = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.caller.post(_this.path.image, {})
                .then(function (result) { return resolve(result); })
                .catch(function (err) { return reject(err); });
        });
    };
    NodeCaller.prototype.setPoints = function (obj) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.caller.post(_this.path.points, obj)
                .then(function (result) { return resolve(result); })
                .catch(function (err) { return reject(err); });
        });
    };
    NodeCaller.prototype.DeleteById = function (id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.caller.delete(_this.path.delPoints(id), {})
                .then(function (result) { return resolve(result); })
                .catch(function (err) { return reject(err); });
        });
    };
    NodeCaller.ctorParameters = function () { return [
        { type: _caller_basic__WEBPACK_IMPORTED_MODULE_1__["BaseCaller"] },
        { type: _caller_path__WEBPACK_IMPORTED_MODULE_3__["CallerPath"] }
    ]; };
    NodeCaller = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])()
    ], NodeCaller);
    return NodeCaller;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'sold-dobot';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_semantic_ui__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-semantic-ui */ "./node_modules/ng2-semantic-ui/dist/public.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _Caller_caller_basic__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Caller/caller.basic */ "./src/app/Caller/caller.basic.ts");
/* harmony import */ var _Caller_caller_path__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Caller/caller.path */ "./src/app/Caller/caller.path.ts");
/* harmony import */ var _Caller_nodeCaller__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Caller/nodeCaller */ "./src/app/Caller/nodeCaller.ts");
/* harmony import */ var _main_cont_main_cont_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./main-cont/main-cont.component */ "./src/app/main-cont/main-cont.component.ts");
/* harmony import */ var _store_main_store_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./store/main-store.service */ "./src/app/store/main-store.service.ts");











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _main_cont_main_cont_component__WEBPACK_IMPORTED_MODULE_9__["MainContComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                ng2_semantic_ui__WEBPACK_IMPORTED_MODULE_3__["SuiModule"]
            ],
            providers: [
                _Caller_caller_basic__WEBPACK_IMPORTED_MODULE_6__["BaseCaller"],
                _Caller_caller_path__WEBPACK_IMPORTED_MODULE_7__["CallerPath"],
                _Caller_nodeCaller__WEBPACK_IMPORTED_MODULE_8__["NodeCaller"],
                _store_main_store_service__WEBPACK_IMPORTED_MODULE_10__["MainStoreService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/main-cont/main-cont.component.css":
/*!***************************************************!*\
  !*** ./src/app/main-cont/main-cont.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".imageObj{\n    width: 800px !important;\n    height: 500px !important;\n    min-width: 800px;\n    min-height: 500px;\n    max-height: 500px;\n    max-width: 800px;\n    /* transform: rotate(180deg); */\n\n    cursor: crosshair;\n}\n.image-hidden{\n    visibility: hidden;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi1jb250L21haW4tY29udC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksdUJBQXVCO0lBQ3ZCLHdCQUF3QjtJQUN4QixnQkFBZ0I7SUFDaEIsaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsK0JBQStCOztJQUUvQixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL21haW4tY29udC9tYWluLWNvbnQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbWFnZU9iantcbiAgICB3aWR0aDogODAwcHggIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDUwMHB4ICFpbXBvcnRhbnQ7XG4gICAgbWluLXdpZHRoOiA4MDBweDtcbiAgICBtaW4taGVpZ2h0OiA1MDBweDtcbiAgICBtYXgtaGVpZ2h0OiA1MDBweDtcbiAgICBtYXgtd2lkdGg6IDgwMHB4O1xuICAgIC8qIHRyYW5zZm9ybTogcm90YXRlKDE4MGRlZyk7ICovXG5cbiAgICBjdXJzb3I6IGNyb3NzaGFpcjtcbn1cbi5pbWFnZS1oaWRkZW57XG4gICAgdmlzaWJpbGl0eTogaGlkZGVuO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/main-cont/main-cont.component.ts":
/*!**************************************************!*\
  !*** ./src/app/main-cont/main-cont.component.ts ***!
  \**************************************************/
/*! exports provided: MainContComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainContComponent", function() { return MainContComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _Caller_nodeCaller__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Caller/nodeCaller */ "./src/app/Caller/nodeCaller.ts");
/* harmony import */ var _store_main_store_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../store/main-store.service */ "./src/app/store/main-store.service.ts");
/* harmony import */ var _Caller_caller_path__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Caller/caller.path */ "./src/app/Caller/caller.path.ts");





var MainContComponent = /** @class */ (function () {
    function MainContComponent(call, mainStore, pathCaller) {
        this.call = call;
        this.mainStore = mainStore;
        this.pathCaller = pathCaller;
        this.headElements = ['ID', 'X position', 'Y position'];
        this.scannedImage = '';
        this.selectedPoints = [];
        this.isUploading = false;
        this.isSoldering = false;
        this.y0 = 0;
        this.currentangle = 0;
        this.currentPosition = {
            x: 0,
            y: 0
        };
    }
    MainContComponent.prototype.ngOnInit = function () {
    };
    MainContComponent.prototype.getImage = function () {
        var _this = this;
        this.isUploading = true;
        this.selectedPoints = [];
        // this.scannedImage = `https://www.aat-corp.com/wp-content/uploads/2019/06/pcb.jpg`;
        this.call.getImage().then(function (data) {
            var res = data.toString().split("/", 10);
            _this.scannedImage = "http://" + _this.pathCaller.ip + "/assets/scanned/" + res['7'];
            console.log(res);
            setTimeout(function () {
                _this.isUploading = false;
            }, 1000);
            // this.ngOnInit();
        })
            .catch(function (e) { alert(e); });
        this.isUploading = false;
    };
    MainContComponent.prototype.onMouse = function (e) {
        this.currentPosition = {
            x: e.offsetX + 10,
            y: e.offsetY - 5
        };
    };
    MainContComponent.prototype.onClikPoint = function () {
        if (this.scannedImage != '') {
            this.selectedPoints.push(this.currentPosition);
        }
    };
    MainContComponent.prototype.onRemove = function (index) {
        this.selectedPoints.splice(index, 1);
    };
    MainContComponent.prototype.solderPoints = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var len, selectedPoint, i, _a, _b, data;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_c) {
                switch (_c.label) {
                    case 0:
                        this.mainStore.isSoldering = true;
                        console.log(this.selectedPoints);
                        selectedPoint = { angleStep: 0, length: 0 };
                        i = 0;
                        _c.label = 1;
                    case 1:
                        if (!(i < this.selectedPoints.length)) return [3 /*break*/, 6];
                        len = i;
                        console.log("for i : " + i);
                        _a = selectedPoint;
                        return [4 /*yield*/, this.calculateAngle(this.selectedPoints[i].x, this.selectedPoints[i].y)];
                    case 2:
                        _a.angleStep = _c.sent();
                        _b = selectedPoint;
                        return [4 /*yield*/, this.calculateLength(this.selectedPoints[i].x, this.selectedPoints[i].y)];
                    case 3:
                        _b.length = _c.sent();
                        return [4 /*yield*/, this.call.setPoints(selectedPoint)];
                    case 4:
                        data = _c.sent();
                        this.sleepDelay(4000 + selectedPoint.angleStep * 100);
                        console.log(data);
                        if (len >= this.selectedPoints.length - 1) {
                            this.mainStore.isSoldering = false;
                        }
                        _c.label = 5;
                    case 5:
                        i++;
                        return [3 /*break*/, 1];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    MainContComponent.prototype.abort = function () {
        this.mainStore.isSoldering = false;
    };
    MainContComponent.prototype.calculateLength = function (x, y) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var y1, x1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        y1 = 500 - y + this.y0;
                        return [4 /*yield*/, this.getX(x)];
                    case 1:
                        x1 = _a.sent();
                        length = Math.sqrt((y1) ^ 2 + (x1) ^ 2);
                        console.log(x, y);
                        length = (length - 3) * 25 / 22;
                        console.log("lenght :  ", length);
                        return [2 /*return*/, Math.round(length)];
                }
            });
        });
    };
    MainContComponent.prototype.getX = function (x) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                if (x < 400) {
                    return [2 /*return*/, 400 - x];
                }
                else {
                    return [2 /*return*/, x - 400];
                }
                return [2 /*return*/];
            });
        });
    };
    MainContComponent.prototype.calculateAngle = function (x, y) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var y1, x1, a, scaleCons, theta;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        y1 = 500 - y + this.y0;
                        return [4 /*yield*/, this.getX(x)];
                    case 1:
                        x1 = _a.sent();
                        a = -1;
                        scaleCons = 500;
                        if (x < 400) {
                            x1 = 400 - x;
                            a = 1;
                            scaleCons = 300;
                        }
                        else {
                            x1 = x - 400;
                        }
                        theta = (Math.atan(x1 / y1) * 180 / Math.PI) * a;
                        console.log("angle : ", theta);
                        theta = theta * scaleCons / 72;
                        console.log("Scaled angle : ", theta);
                        return [2 /*return*/, Math.round(theta)];
                }
            });
        });
    };
    MainContComponent.prototype.sleepDelay = function (milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds) {
                break;
            }
        }
    };
    MainContComponent.ctorParameters = function () { return [
        { type: _Caller_nodeCaller__WEBPACK_IMPORTED_MODULE_2__["NodeCaller"] },
        { type: _store_main_store_service__WEBPACK_IMPORTED_MODULE_3__["MainStoreService"] },
        { type: _Caller_caller_path__WEBPACK_IMPORTED_MODULE_4__["CallerPath"] }
    ]; };
    MainContComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-cont',
            template: __webpack_require__(/*! raw-loader!./main-cont.component.html */ "./node_modules/raw-loader/index.js!./src/app/main-cont/main-cont.component.html"),
            styles: [__webpack_require__(/*! ./main-cont.component.css */ "./src/app/main-cont/main-cont.component.css")]
        })
    ], MainContComponent);
    return MainContComponent;
}());

var Point = /** @class */ (function () {
    function Point() {
    }
    return Point;
}());
var SoldPoint = /** @class */ (function () {
    function SoldPoint() {
    }
    return SoldPoint;
}());


/***/ }),

/***/ "./src/app/store/main-store.service.ts":
/*!*********************************************!*\
  !*** ./src/app/store/main-store.service.ts ***!
  \*********************************************/
/*! exports provided: MainStoreService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainStoreService", function() { return MainStoreService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MainStoreService = /** @class */ (function () {
    function MainStoreService() {
        this.isSoldering = false;
    }
    MainStoreService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], MainStoreService);
    return MainStoreService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/tharudg/Desktop/DOBOT/sold-dobot/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map