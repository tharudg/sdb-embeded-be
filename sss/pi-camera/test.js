const PiCamera = require('./index')

const myCamera = new PiCamera({
  mode: 'video',
  output: '//home//pi//Videos//video.h264',
  width: 640,
  height: 480,
  timeout: 20000,
  nopreview: true,
  vflip: true,
})

const old = myCamera.configToArray();
const _ = myCamera._configToArray();

old.map((value, index) => {
  if (value !== _[index])
    throw new Error('Shit don\'t match!')
})