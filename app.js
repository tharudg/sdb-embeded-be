const server = require('./server');
const imgRoutes = require('./routes/imgRoutes');
const pointsRoutes = require('./routes/pointsRoutes');
const initRoutes = require('./routes/initialize');
server.router.post('/scanned/image', imgRoutes.captureImage);
server.router.post('/points', pointsRoutes.controlServo);
server.router.post('/init',initRoutes.controlServo);
server.start();