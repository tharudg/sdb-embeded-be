const Hapi = require('hapi');

// Create a server with a host and port
const server=Hapi.server({
    port:5000,
    routes:{    
    cors: {
        origin: ['*'],
        additionalHeaders: ['cache-control', 'x-requested-with', 'token']
    }}
});

var routes = [];



async function start() {
    console.log('server started now')
    try {
        for (let route of routes) {
            console.log(route)
            server.route(route);

        }
        await server.start();
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }

    console.log('Server running at:', server.info.uri);
};

module.exports = {
    start: start,

    router: {
        get: function (path, handler) {
            routes.push({
                method:'GET',
                path:path,
                handler:handler
            })
        },
        post: function (path,  handler) {
            routes.push({
                method:'POST',
                path:path,
                handler: handler
            })
        },
    }
};
