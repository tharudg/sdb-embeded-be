const Gpio = require('pigpio').Gpio;
const request = require('request');

const motor1 = new Gpio(13, {mode: Gpio.OUTPUT});
const motor2 = new Gpio(18, {mode: Gpio.OUTPUT});


MAX = 2500;
MIN = 700;

current = 1200;

let angleServo = (angle,motor)=>{
    return motor.servoWrite(10*angle + 700);
}


let delay = async(milliseconds) =>{
    try{
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }
    catch{return 0}
  }



module.exports = {
    controlServo:async (req,h)=>{
       try{
        await controlStepper(req.payload.angleStep)
        angleServo(req.payload.length,motor1);
        angleServo(Math.abs(90-req.payload.length)*2, motor2); 

        console.log("angle : ",req.payload.angleStep);
        console.log("length : ",req.payload.length);
        await delay(3000);

        let returnAngle = req.payload.angleStep * -1
        await controlStepper(returnAngle)
        console.log("end returning angle : ",returnAngle )
        return req.payload.angleStep
       }
       catch{
           console.log(error);
           return error
       }
       
    }


   
}


function controlStepper(angle){

    return new Promise((resolve, reject)=>{
        angleServo(70,motor2);
        request('http://localhost:2020/test/'+ angle,(error, response, body)=>{
            if(error){
                reject(error)
            }

            try{
                resolve(body)
            }catch(e){
                reject(e);
            }
      
        })
    });
}