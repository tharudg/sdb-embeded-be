const { StillCamera } = require("pi-camera-connect");
const fs =require("fs");
const path = require('path');

module.exports ={
    
    captureImage:async (req, h) =>  { 
        try{
            console.log("camera started")

            const stillCamera = new StillCamera();
     
            const image = await stillCamera.takeImage();
            filepath = path.join(__dirname,'../assets/scanned/'+Date.now()+'.jpg');
            console.log(filepath);
            fs.writeFileSync(filepath , image);
            
            return filepath

        }catch (e) {
            console.log(e)
            console.log("camera not started")
            return responseMsgs.badRequest(h,e)
        }

    },
}
 
